This is a project written entirely by myself. The purpose of it is to create a way to track and display current outages to the company.

Due to server restrictions, Vue couldn't be installed so a javascript library is used to emulate the functionality so that single-file components could be used.

See the screenshots folder for what the app looks like