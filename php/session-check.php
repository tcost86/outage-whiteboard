<?php
$user = "";
$perm = -1;

 // Determine if SSL or not
$http = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http");

// IE nocportal-t
$host = $_SERVER['HTTP_HOST'];
session_start();

if(isset($_SESSION["username"])) {
    $user = $_SESSION["username"];
    $perm = $_SESSION["perm"];
}