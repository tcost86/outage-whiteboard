<?php
/**
 * 
 * Returns a key for Remedy using the noc_owb user
 * 
 */
function getRemedyKeyOWB() {
    $err = false;
	$err_text = "";
	$curl = curl_init();
	$link = (gethostname() == "nympnpsv03t.mediacomcorp.com") ? "http://nymprarv:8008/api/jwt/login":"http://iaalrarv:8008/api/jwt/login";
	curl_setopt($curl, CURLOPT_URL, $link);
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, "username=noc_owb&password=");
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$k = curl_exec($curl);

	if(!curl_errno($curl)) {
		switch($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
			case 200:
				break;
			default:
				$err = true;
				$err_text = "error: unexpected HTTP code: ".$http_code."\n";
		}
	}

	curl_close($curl);

	if(!$err) {
		return $k;
	} else {
		return $err_text;
	}
}


/**
 * 
 * Sorts an array by specified column
 * 
 * @param field The column to sort by
 * @param array Array to sort
 * @param direction Direction to sort (asc or desc)
 * @return array Sorted array elements
 * 
 */
function sortThis($field, &$array, $direction = "asc")
{
    @usort($array, create_function('$a, $b', '
        $a = $a["' . $field . '"];
        $b = $b["' . $field . '"];

        if ($a == $b) return 0;

        $direction = strtolower(trim($direction));

        return ($a ' . ($direction == 'desc' ? '>' : '<') .' $b) ? -1 : 1;
    '));

    return true;
}

/**
 * 
 * Returns ticket overdue status
 * 
 * @param ETR Estimated Resolution Date from ticket
 * @param perm Logged in users' permission level
 * @return array Icon and CSS class for status
 * 
 */
function getOverdueStatus($ETR, $perm = 0) 
{
    $now = date("Y-m-d H:i");
    $minLeft = intval((strtotime($ETR) - strtotime($now))/60);
    $me = array(
        "class" =>  "",
        "icon"  =>  ""  
    );

    if(is_null($ETR) || $ETR == "" || $perm < 1) {
        $me["class"] = "is-status-unknown";
        $me["icon"] = "fas fa-question";
    } else {
        if($minLeft == "") {
            $me["class"] = "is-status-unknown";
            $me["icon"] = "fas fa-question";
        } else if($minLeft < 0) {
            $me["class"] = "is-status-critical";
            $me["icon"] = "fas fa-bell";
        } else if($minLeft <= 15) {
            $me["class"] = "is-status-warning";
            $me["icon"] = "fas fa-exclamation-circle";
        } else if($minLeft <= 30) {
            $me["class"] = "is-status-notice";
            $me["icon"] = "fas fa-exclamation-triangle";
        } else {
            $me["class"] = "is-status-ok";
            $me["icon"] = "fas fa-check-circle";
        }
    }

    return $me;
}

/**
 * 
 * Returns ticket next update in days/hours/minutes
 * 
 * @param ETR Estimated Resolution Date from ticket
 * @return string Amount of time until due or if overdue
 * 
 */
function getNextUpdateTime($ETR) 
{
    $me = "";
    $due = "";
    $now = date("Y-m-d H:i");

    if(!is_null($ETR) && $ETR != "") {
        $minLeft = intval((strtotime($ETR) - strtotime($now))/60);
        $dueHours1 = intval(str_replace('-', '', $minLeft) /60);
        $dueHours = intval($dueHours1 % 24)."H";
        $dueMinutes = intval(str_replace('-', '', $minLeft) % 60)."M";
        $dueDays = intval($dueHours1 / 24)."D";

        if($dueDays > 0) {
            $due = $dueDays." ".$dueHours." ".$dueMinutes;
        } else if($dueHours > 0) {
            $due = $dueHours." ".$dueMinutes;
        } else {
            $due = $dueMinutes;
        }

        if($minLeft < 0) {
            $me = "Past Due: ".$due;
        } else {
            $me = "Due In: ".$due;
        }
    }

    return $me;
}

/**
 * 
 * Returns ticket next update time
 * 
 * @param ETR Estimated Resolution Date from ticket
 * @return string Next update date
 * 
 */
function getNextDate($ETR) 
{
    $next = "No Target Date Set";

    if(!is_null($ETR) && $ETR != "") {
        $next = date("n/d/y g:iA", strtotime($ETR));
    }

    return $next;
}

/**
 * 
 * Returns ticket region data
 * 
 * @param system System_AF from ticket
 * @param market Market_AF from ticket
 * @param manual Region when ticket is manually entered
 * @return array Region text and HTML class
 * 
 */
function getRegion($system, $market, $manual = "") 
{
    $me = array(
        "text"  =>  "",
        "class" =>  ""
    );

    if($manual != "") {
        $market = $manual;
        $system = $manual;
    }

    if($system == "" && $market != "Company-Wide") {
        $me["text"] = "Unknown Region";
    } else {
        if($market == "Company-Wide") {
            $me["text"] = "Company-Wide";
            $me["class"] = "is-primary";
        } else {
            if($system == "Coastal") {
                $me["text"] = "Coastal";
                $me["class"] = "is-coastal";
            } else if($system == "Capital") {
                $me["text"] = "Capital";
                $me["class"] = "is-blue";
            } else if($system == "Lincoln") {
                $me["text"] = "Lincoln";
                $me["class"] = "is-lincoln";
            } else if($system == "Lakes") {
                $me["text"] = "Lakes";
                $me["class"] = "is-lakes";
            } else {
                $me["text"] = "Coporate";
                $me["class"] = "is-corporate";
            }
            
        }
    }

    return $me;
}

/**
 * 
 * Returns ticket last update date
 * 
 * @param data WorkInfo from ticket
 * @return string Region text and HTML class
 * 
 */
function getLastUpdate($data) 
{
    $me = "None";

    if(!empty($data)) {
        $me = date("n/d/y g:iA", strtotime($data[0]["values"]["Submit Date"]));
    }

    return $me;
}

/**
 * 
 * Returns a CI's ping status
 * 
 * @param CI CI name from ticket
 * @param perm Logged in users' permission level
 * @param canPing CI Ping field on CIU data
 * @return array Ping HTML class and text
 * 
 */
function getCIPing($CI, $canPing, $perm = 0) 
{
    $me = array(
        "class" => "is-light",
        "text"  => "Ping: Not Monitored"
    );

    if($perm > 0 && $canPing == "Yes") {
        $CI = trim($CI);
        $q = run(dbCon("nocportal_network_elements"), "SELECT primary_access FROM DEVICES WHERE device_name = '$CI'");

        if($q->rowCount() == 1) {
            $d = $q->fetch();
            $ip = trim($d["primary_access"]);
            if($ip == "" || $ip == null) {
                $me["class"] = "is-light";
                $me["text"] = "Ping: Not Monitored";
            } else {
                $pingresult = exec("/usr/bin/ping -c 1 -W 2 $ip", $outcome, $s);
                if(0 == $s) {
                    $me["class"] = "is-danger";
                    $me["text"] = "Ping: Unresponsive";
                } else {
                    $me["class"] = "is-success";
                    $me["text"] = "Ping: OK";
                }
            }
        }
    }

    return $me;
}

/**
 * 
 * Determines if the ticket has Carrier Ops impact or not
 * 
 * @param summary Summary line of the INC ticket
 * @param cops COPS Agg under AFSystem1
 * @return array Ping HTML class and text
 * 
 */
function isCarrier($summary, $cops = 0) 
{
    $me = false;

    if((preg_match("/\bCarrier\b/", $summary)) || $cops == 1) {
        $me = true;
    }

    return $me;
}

/**
 * 
 * Returns tag style based on the CIU type
 * 
 * @param ciu CIU type
 * @return array Tag HTML class and CIU type text
 * 
 */
function getCIUType($ciu) 
{
    $me = array(
        "class" => "",
        "text"  =>  $ciu
    );

    if($ciu == "Tracking Only") {
        $me["class"] = "is-success";
    } else if($ciu == "Unscheduled Partial") {
        $me["class"] = "is-warning";
    } else if($ciu == "Unscheduled Full") {
        $me["class"] = "is-danger";
    } else {
        $me["class"] = "is-link";
    }

    return $me;
}

/**
 * 
 * Returns the duration of a ticket
 * 
 * @param start Start time of outage (from CIU)
 * @param end End time of outage or current time if outage still open
 * @return string Duration in hours/min/seconds
 * 
 */
function getDuration($start, $end) 
{
    $duration = intval((strtotime($end) - strtotime($start))/60);
    $duration_hours = intval($duration / 60)."H";
    $duration_minutes = intval($duration % 60). "M";

    return $duration_hours." ".$duration_minutes;
}