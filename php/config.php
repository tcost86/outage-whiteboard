<?php
date_default_timezone_set("America/Chicago");
require "/var/www/common/php/database.php";
require "helpers.php";

// Get content type of incoming request
$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]):"";

// Get method (post, get) of incoming request
$methodType = isset($_SERVER["REQUEST_METHOD"]) ? trim($_SERVER["REQUEST_METHOD"]):"";

// If proper type and POST, get the data and process
if($contentType === "application/json" && $methodType === "POST") {

    // Raw POST data
    $postData = trim(file_get_contents("php://input"));
    $postData = json_decode($postData);
    
    // Get action to perform and process data
    // NOTE: Any data returned should be JSON
    switch($postData->action) {


        /**
         * Action: writeEscalation
         * 
         * Adds an escalation entry into specified ticket #
         * 
         * @param ticket Remedy ticket #
         * @param data Escalation data for the ticket
         * @return JSON Returns if update was successful or not
         * 
         */
        case "writeEscalation":
            $ticketNumber = $postData->ticket;
            $escalationData = $postData->data;
            $term = ($escalationData->contacted == "yes") ? "contacted":"notified";

            $workLogEntry = $escalationData->type." Escalation - ".$escalationData->name." has been $term by: ".$escalationData->method.".";
            $workLogSummary = "U:".$escalationData->user." T:".$escalationData->type." M:".$escalationData->method." N:".$escalationData->name;

            $query = array("q" => "'Incident Number' = \"$ticketNumber\"", "limit" => 1);
			$getRequestID = queryRemedy("HPD", $query, "Request ID");

            $remedyKey = getRemedyKeyOWB();
            $header = array(
				"Content-Type: application/json",
				"Authorization: ".$remedyKey
			);

            $curl = curl_init();
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");

			$link1 = (gethostname() == "nympnpsv03t.mediacomcorp.com") ? "http://nymprarv:8008/api/arsys/v1/entry/HPD:IncidentInterface/":"http://iaalrarv:8008/api/arsys/v1/entry/HPD:IncidentInterface/";
			$link1 .= $getRequestID["entries"][0]["values"]["Request ID"];
			curl_setopt($curl, CURLOPT_URL, $link1);
			$wlData = array(
				"values" => array(
                    "z1D_Details"           => $workLogEntry,
                    "z1D_WorklogDetails"    => $workLogSummary,
					"z1D_Activity_Type"     => "Working Log",
					"z1D Action"            => "MODIFY",
					"z1D_Secure_Log"        => "Yes"
                )
			);

			$wlData = json_encode($wlData);

			curl_setopt($curl, CURLOPT_POSTFIELDS, $wlData);
			$data = curl_exec($curl);

			$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            curl_close($curl);
			if($http_code == 204) {
		        echo json_encode("ok");
			} else {
				echo json_encode("error");
			}
            break;

        

        /**
         * Action: getProblems
         * 
         * Gets active PBI and PKEs for the ProblemList form
         * 
         * @return JSON Returns ticket data
         * 
         */
        case "getProblems":
            $returnData = array();
            $now = date("Y-m-d H:i");
            
            $getPBIs = run(dbCon("nocportal_tools_suite"), "SELECT * FROM OW_pbi ORDER BY submit_date DESC");
            $rows = $getPBIs->rowCount();

            if($rows > 0) {

                while($row = $getPBIs->fetch()) {
                    $ticketNumber = $row["ticket_number"];
                    $ticketType = substr($ticketNumber, 0, 3);
                    $params = array();
                    $form = "";

                    if($ticketType == "PBI") {
                        $params = array("q" => "'Problem Investigation ID' = \"$ticketNumber\"");
                        $form = "PBM:ProblemInterface";
                    } else {
                        $params = array("q" => "'Known Error ID' = \"$ticketNumber\"");
                        $form = "PBM:KnownErrorInterface";
                    }

                    $ticketData = queryRemedy(
                        $form,
                        $params,
                        "Description,Assigned Group"
                    );

                    $duration = intval((strtotime($now) - strtotime($row["submit_date"]))/60);
                    $duration_hours = intval($duration / 60)."H";
                    $duration_minutes = intval($duration % 60). "M";

                    $returnData[] = array(
                        "type"      =>  $ticketType,
                        "tick"      =>  $ticketNumber,
                        "sum"       =>  $ticketData["entries"][0]["values"]["Description"],
                        "dur"       =>  $duration_hours." ".$duration_minutes,
                        "sub"       =>  date("n/d/y g:i A", strtotime($row["submit_date"])),
                        "creator"   =>  $row["submitter"],
                        "group"     =>  $ticketData["entries"][0]["values"]["Assigned Group"]
                    );
                }

            }

            echo json_encode($returnData);
            break;



        /**
         * Action: removeTicket
         * 
         * Removes specified ticket from tracking in the OWB
         * 
         * @param ticket Ticket #
         * @return JSON Returns successfull deletion
         * 
         */
        case "removeTicket":
            $ticketNumber = $postData->ticket;
            $toolsSuiteDB = dbCon("nocportal_tools_suite");
            $ticketType = substr($ticketNumber, 0, 3);
            $query = "";

            if($ticketType == "INC") {
                $query = "DELETE FROM OW_incident WHERE ticket_number = '$ticketNumber'";
            } else {
                $query = "DELETE FROM OW_pbi WHERE ticket_number = '$ticketNumber'";
            }

            $do = run($toolsSuiteDB, $query);

            if($do) {
                echo json_encode("ok");
            } else {
                echo json_encode("error");
            }

            break;



        /**
         * Action: getClosedTickets
         * 
         * Gets closed tickets and returns data to populate the Closed Alerts.
         * 
         * @param perm User permission level
         * @param view Time selected (24 hours or 7 days)
         * @return JSON Returns ticket data
         * 
         */
        case "getClosedTickets":
            $userPerm = $postData->perm;
            $userView = $postData->view;
            $returnData = array();
            $paramsCIU = array();

            // Setup Remedy CIU query based on what view is being used
            if($userView == "24") {
                $paramsCIU = array(
                    "q" =>  "'Unavailability Status' = \"Restored\" AND 'Actual End Date' > $\TIMESTAMP$ - (24*60*60) AND 'Resolution Code' != \"Cancelled\" AND 'Unavailability Class' = \"Incident\" AND 'Assignment Status' = \"Completed\""
                );
            } else if($userView == "7") {
                $paramsCIU = array(
                    "q" =>  "'Unavailability Status' = \"Restored\" AND 'Actual End Date' > $\TIMESTAMP$ - (24*60*60*7) AND 'Resolution Code' != \"Cancelled\" AND 'Unavailability Class' = \"Incident\" AND 'Assignment Status' = \"Completed\""
                );
            }

            $closedCIU = queryRemedy(
                "AST:CI%20Unavailability",
                $paramsCIU,
                "Cross Reference ID,Actual Start Date,Actual End Date,Unavailability Type"
            );

            // Loop through CIUs and get associated INC data for each
            if(@count($closedCIU["entries"]) > 0) {
                $con = dbCon("nocportal_tools_suite");

                for($i = 0; $i < count($closedCIU["entries"]); $i++) {
                    $INC = $closedCIU["entries"][$i]["values"]["Cross Reference ID"];
                    $CIUType = $closedCIU["entries"][$i]["values"]["Unavailability Type"];
                    $checkINC = run($con, "SELECT * FROM `OW_incident` WHERE `ticket_number` = '$INC'")->rowCount();
                    $paramsINC = array("q" => "'Incident Number' = \"$INC\"");

                    if($checkINC > 0) {
                        run($con, "DELETE FROM `OW_incident` WHERE `ticket_number` = '$INC'");
                    }

                    $getINCData = queryRemedy(
                        "HPD:Help%20Desk",
                        $paramsINC,
                        "Estimated Resolution Date,Market_AF,System_AF,Description,Submitter,Status,COPS Agg,Resolution"
                    );

                    $WorkParams = array("q" => "'Incident Number' = \"$INC\" AND 'View Access' = \"Public\"", "limit" => 1);

                    $getWorkData = queryRemedy(
                        "WorkInfo",
                        $WorkParams,
                        "Submit Date,Submitter"
                    );

                    @$ETR = $getINCData["entries"][0]["values"]["Estimated Resolution Date"];
                    @$system = $getINCData["entries"][0]["values"]["System_AF"];
                    @$market = $getINCData["entries"][0]["values"]["Market_AF"];
                    @$summary = $getINCData["entries"][0]["values"]["Description"];
                    @$carrierAgg = $getINCData["entries"][0]["values"]["COPS Agg"];
                    @$ticketSubmitter = $getINCData["entries"][0]["values"]["Submitter"];
                    @$startTime = $closedCIU["entries"][$i]["values"]["Actual Start Date"];
                    @$endTime = $closedCIU["entries"][$i]["values"]["Actual End Date"];
                    @$lastSubmitter = ($getWorkData["entries"][0]["values"]["Submitter"] == "Remedy Application Service") ? "remedy":$getWorkData["entries"][0]["values"]["Submitter"];

                    // Functions below are in helpers.php
                    $returnData[] = array(
                        "inc"       =>  $INC,
                        "sum"       =>  $summary,
                        "sub"       =>  $ticketSubmitter,
                        "next"      =>  getNextDate($ETR),
                        "last"      =>  getLastUpdate($getWorkData["entries"]),
                        "last_sub"  =>  $lastSubmitter,
                        "due"       =>  "",
                        "ping"      =>  "",
                        "carrier"   =>  isCarrier($summary, $carrierAgg),
                        "region"    =>  getRegion($system, $market),
                        "status"    =>  getOverdueStatus($ETR, $userPerm),
                        "ciutype"   =>  getCIUType($CIUType),
                        "closed"    =>  date("n/d/y g:iA", strtotime($endTime)),
                        "dur"       =>  getDuration($startTime, $endTime),
                        "res"       =>  $getINCData["entries"][0]["values"]["Resolution"]
                    );
                }

            }

            @sortThis("closed", $returnData, "desc");
            echo json_encode($returnData);
            break;



        /**
         * Action: getActiveTickets
         * 
         * Gets active INC tickets (open CIUs on them) and returns data to populate
         * the Active Alerts. This also queries the DB for any manually added tickets.
         * 
         * @param perm User permission level
         * @param region Region filter
         * @param view View filter (full or carrier)
         * @return JSON Returns ticket data
         * 
         */
        case "getActiveTickets":
            $userPerm = $postData->perm;
            $userRegion = $postData->region;
            $userView = $postData->view;
            $toolsSuiteDB = dbCon("nocportal_tools_suite");
            $returnData = array();
            $INCData = array();

            // Open CIUs in Remedy
            $CIUParams = array("q" => "'Unavailability Status' = \"Current Unavailability\" AND 'Unavailability Class' = \"Incident\" AND 'Data Set Id' = \"BMC.ASSET\"");
            $activeCIUS = queryRemedy(
                "CIU",
                $CIUParams,
                "Cross Reference ID,CI Ping,Unavailability Type"
            );

            // Tickets submitted by employees (no active CIUs on them)
            $manualINCs = run($toolsSuiteDB, "SELECT ticket_number, region FROM `OW_incident` WHERE `submit_date` != '0000-00-00 00:00:00'");

            // Loop through and store each INC# retrieved
            if($activeCIUS["entries"] && !empty($activeCIUS["entries"])) {
                foreach($activeCIUS["entries"] as $key => $value) {
                    $inc = $value["values"]["Cross Reference ID"];

                    $INCData[] = array(
                        "inc"           =>  $inc, 
                        "ping"          =>  $value["values"]["CI Ping"], 
                        "type"          =>  $value["values"]["Unavailability Type"],
                        "reg"           =>  "",
                        "event_type"    =>  "Large Event"
                    );
                }
            }

            if($manualINCs->rowCount() > 0) {
                while($row = $manualINCs->fetch()) {
                    $INCData[] = array(
                        "inc"           =>  $row["ticket_number"], 
                        "ping"          =>  "", 
                        "type"          =>  "Tracking Only",
                        "reg"           =>  $row["region"],
                        "event_type"    =>  ""
                    );
                }
            }

            // Loop through all INCs and get values needed
            foreach($INCData as $key => $value) {
                $ticket = $value["inc"];
                $CIPing = $value["ping"];
                $CIUType = $value["type"];
                $manualRegion = $value["reg"];
                $canAdd = false;

                // Get necessary ticket data
                $INCParams = array("q" => "'Incident Number' = \"$ticket\"");
                $getINCData = queryRemedy(
                    "HPD:Help%20Desk",
                    $INCParams,
                    "Estimated Resolution Date,Market_AF,System_AF,Description,HPD_CI,Submitter,Status,COPS Agg"
                );

                // Ignore this ticket and delete from DB if manually added and set to "Resolved"
                if($CIUType == "Tracking Only" && $getINCData["entries"][0]["values"]["Status"] == "Resolved") {
                    run($toolsSuiteDB, "DELETE FROM OW_incident WHERE ticket_number = '$ticket'");
                    continue;
                }

                // Get last public work log entry (if any)
                $WorkParams = array("q" => "'Incident Number' = \"$ticket\" AND 'View Access' = \"Public\"", "limit" => 1);
                $getWorkData = queryRemedy(
                    "WorkInfo",
                    $WorkParams,
                    "Submit Date,Submitter"
                );

                @$ETR = $getINCData["entries"][0]["values"]["Estimated Resolution Date"];
                @$system = $getINCData["entries"][0]["values"]["System_AF"];
                @$market = $getINCData["entries"][0]["values"]["Market_AF"];
                @$summary = $getINCData["entries"][0]["values"]["Description"];
                @$carrierAgg = $getINCData["entries"][0]["values"]["COPS Agg"];
                @$CI = $getINCData["entries"][0]["values"]["HPD_CI"];
                @$ticketSubmitter = ($getINCData["entries"][0]["values"]["Submitter"] == "Remedy Application Service") ? "remedy":$getINCData["entries"][0]["values"]["Submitter"];
                @$lastSubmitter = ($getWorkData["entries"][0]["values"]["Submitter"] == "Remedy Application Service") ? "remedy":$getWorkData["entries"][0]["values"]["Submitter"];
                $region = getRegion($system, $market, $manualRegion);
                $isCarrier = isCarrier($summary, $carrierAgg);

                // If not manually added, check & add into DB if necessary
                if($CIUType != "Tracking Only") {
                    $isTracked = run($toolsSuiteDB, "SELECT * FROM OW_incident WHERE ticket_number = '$ticket'");

                    if($isTracked->rowCount() == 0) {
                        run($toolsSuiteDB, "INSERT INTO OW_incident (`ticket_number`, `submit_date`) VALUES ('$ticket', '0000-00-00 00:00:00')");
                    }
                }
                
                // Functions below are in helpers.php
                $thisData = array(
                    "inc"       =>  $ticket,
                    "sum"       =>  $summary,
                    "sub"       =>  $ticketSubmitter,
                    "next"      =>  getNextDate($ETR),
                    "last"      =>  getLastUpdate($getWorkData["entries"]),
                    "last_sub"  =>  $lastSubmitter,
                    "due"       =>  getNextUpdateTime($ETR),
                    "ping"      =>  getCIPing($CI, $CIPing, $userPerm),
                    "carrier"   =>  $isCarrier,
                    "region"    =>  $region,
                    "status"    =>  getOverdueStatus($ETR, $userPerm),
                    "ciutype"   =>  getCIUType($CIUType)
                );

                // Verify we can display this ticket based on $userRegion and $userView
                if($userRegion == $region["text"] || $userRegion == "All Regions") {
                    $canAdd = true;
                }

                if($userView == "Carrier View" && !$isCarrier) {
                    $canAdd = false;
                }

                if($canAdd) {
                    $returnData[] = $thisData;
                }
            }

            @sortThis("due", $returnData, "desc");
            echo json_encode($returnData);
            break;


        /**
         * Action: submitTicket
         * 
         * Handles manually entered INC/PBI/PKE tickets ("AddTicket" form)
         * 
         * @param ticket Ticket #
         * @param region Region (for INC tickets only)
         * @param type Type of ticket expected from form  
         * @return JSON Returns if ticket was successfully added, errors if not
         * 
         */
        case "submitTicket":
            $ticketNumber = trim($postData->ticket);
            $region = ($postData->region) ? $postData->region:"na";
            $submittedType = $postData->type;
            $submitter = $postData->submitter;
            $ticketType = substr($ticketNumber, 0, 3);
            $now = date("Y-m-d H:i");
            $form = "";
            $remedyQuery = "";
            $dbQuery = "";
            $db = "";

            if($submittedType == "INC") {
                $form = "HPD";
                $remedyQuery = array("q" => "'Incident Number' = \"$ticketNumber\"");
                $db = "OW_incident";
            } else if($submittedType == "PBI/PKE") {
                if($ticketType == "PBI") {
                    $form = "PBM:Problem%20Investigation";
                    $remedyQuery = array("q" => "'Problem Investigation ID' = \"$ticketNumber\"");
                } else if($ticketType == "PKE") {
                    $form = "PBM:Known%20Error";
                    $remedyQuery = array("q" => "'Known Error ID' = \"$ticketNumber\"");
                }

                $db = "OW_pbi";
            }

            // Check if ticket submitted is expected type from form
            if($submittedType == "INC" && $ticketType != "INC" || $submittedType == "PBI/PKE" && ($ticketType != "PBI" && $ticketType != "PKE")) {
                echo json_encode("$ticketType is not a valid ticket for this form. Please use $submittedType tickets.");
                break;
            }

            // Check if ticket exists in Remedy
            $ticketCheck = queryRemedy($form, $remedyQuery);

            if(count($ticketCheck["entries"]) == 0) {
                echo json_encode("$ticketNumber wasn't found in Remedy. Please check what you typed and try again.");
                break;
            }

            // Check if INC ticket has a CIU (only tickets with no CIUs should be manually entered)
            if($submittedType == "INC") {
                $CIUParams = array(
                    "q" => "'Unavailability Status' = \"Current Unavailability\" AND 'Unavailability Class' = \"Incident\" AND 'Data Set Id' = \"BMC.ASSET\" AND 'Cross Reference ID' = \"$ticketNumber\""
                );
                $activeCIUS = queryRemedy("CIU", $CIUParams);

                if(count($activeCIUS["entries"]) > 0) {
                    echo json_encode("$ticketNumber has an active CIU associated with it. Only tickets without active CIUs should be manually entered.");
                    break;
                }
            }

            // Check if already in DB
            $checkDB = run(dbCon("nocportal_tools_suite"), "SELECT * FROM $db WHERE ticket_number = '$ticketNumber'")->rowCount();

            if($checkDB > 0) {
                echo json_encode("$ticketNumber is already being tracked!");
                break;
            }

            // Add into tracking if no errors up until now
            if($ticketType == "INC") {
                $dbQuery = "INSERT INTO OW_incident (`ticket_number`, `submit_date`, `region`) VALUES ('$ticketNumber', '$now', '$region')";
            } else {
                $dbQuery = "INSERT INTO OW_pbi (`ticket_number`, `submit_date`, `submitter`) VALUES ('$ticketNumber', '$now', '$submitter')";
            }

            run(dbCon("nocportal_tools_suite"), $dbQuery);
            echo json_encode("ok");
            break;


        /**
         * Action: loadTicketDetails
         * 
         * Gets data for the "TicketDetails" form
         * 
         * @param ticket Ticket #
         * @param type Type of ticket coming in (active or closed)
         * @return JSON Returns ticket data
         * 
         */
        case "loadTicketDetails":
            $ticketNumber = $postData->ticket;
            $ticketAction = $postData->type;
            $ticketType = "";
            $ticketCIUType = "";
            $impactCounts = array();
            $start = "";
            $end = date("Y-m-d H:i");
            $ignoredCIUFields = array(
                "Total Escalation Level",
                "Total OLA AcknowledgeEsc Level",
                "Total OLA Resolution Esc Level",
                "Total SLA Response Esc Level",
                "Total UC Time Elapsed",
                "US Cellular, MO",
                "OLA Resolution Esc Level",
                "OLA Acknowledge Esc Level",
                "OLA Hold Duration Seconds",
                "Escalation OLA Counter",
                "Estimated Duration Seconds",
                "Extended Scheduled Downtime",
                "Actual Duration Seconds",
                "Time Between System Incidents",
                "Extended Downtime UA",
                "Extended Downtime"
            );

            // Get necessary ticket data
            $INCParams = array("q" => "'Incident Number' = \"$ticketNumber\"");
            $getINCData = queryRemedy(
                "HPD:Help%20Desk",
                $INCParams,
                "Estimated Resolution Date,Market_AF,System_AF,Description,Submitter,Last Modified By,Status,Customer_Experience,Submit Date,Total Count,Done,Resolution"
            );

            // If active ticket is resolved, throw error and do nothing else
            // Resolved tickets will drop from the Active Alerts
            if(@$getINCData["entries"][0]["values"]["Status"] == "Resolved" && @$ticketAction == "active") {
                echo json_encode("resolved");
                break;
            }

            // Determine the ticket type (tracking or an outage)
            @$isDone = $getINCData["entries"][0]["values"]["Done"];
            @$totalCounts = $getINCData["entries"][0]["values"]["Total Count"];
            
            if(!is_null($isDone) && !is_null($totalCounts) || is_null($totalCounts)) {
                $ticketType = "tracking";
            } else {
                $ticketType = "outage";
            }

            // Get necessary CIU data (mostly impact counts)
            $CIUParams = array("q" => "'Unavailability Class' = \"Incident\" AND 'Cross Reference ID' = \"$ticketNumber\"", "limit" => 1);
            $getCIUData = queryRemedy(
                "AST:CI%20Unavailability",
                $CIUParams
            );

            // Get last public work log entry (if any)
            $WorkParams = array("q" => "'Incident Number' = \"$ticketNumber\" AND 'View Access' = \"Public\"", "limit" => 1);
            $getWorkData = queryRemedy(
                "WorkInfo",
                $WorkParams,
                "Submit Date,Submitter,Detailed Description"
            );

            @$ETR = $getINCData["entries"][0]["values"]["Estimated Resolution Date"];
            @$lastSubmitter = ($getWorkData["entries"][0]["values"]["Submitter"]) ? $getWorkData["entries"][0]["values"]["Submitter"]:$getINCData["entries"][0]["values"]["Last Modified By"];
            @$lastUpdate = (count($getWorkData["entries"]) == 0) ? "No update has been entered yet.":$getWorkData["entries"][0]["values"]["Detailed Description"];
            @$market = ($getINCData["entries"][0]["values"]["Market_AF"] == null) ? "None":$getINCData["entries"][0]["values"]["Market_AF"];
            @$ticketSubmitter = ($getINCData["entries"][0]["values"]["Submitter"] == "Remedy Application Service") ? "remedy":$getINCData["entries"][0]["values"]["Submitter"];
            @$customerExperience = (is_null($getINCData["entries"][0]["values"]["Customer_Experience"])) ? "No Customer Experience entered or this is not declared an outage.":$getINCData["entries"][0]["values"]["Customer_Experience"];
            @$ticketStatus = $getINCData["entries"][0]["values"]["Status"];
            @$eventType = "Standard Event";

            if($ticketType == "outage") {
                @$start = $getCIUData["entries"][0]["values"]["Actual Start Date"];
                $ticketCIUType = $getCIUData["entries"][0]["values"]["Unavailability Type"];
            } else {
                @$start = ($getCIUData["entries"][0]["values"]["Actual Start Date"]) ? $getCIUData["entries"][0]["values"]["Actual Start Date"]:$getINCData["entries"][0]["values"]["Submit Date"];
                $ticketCIUType = "Tracking Only";
            }

            if($ticketStatus == "Resolved") {
                @$end = $getCIUData["entries"][0]["values"]["Actual End Date"];
                $ticketCIUType = "Completed";
                $ticketType = "resolved";
            }

            // Input impact counts if ticket has CIU
            if(count($getCIUData["entries"]) > 0) {

                foreach($getCIUData["entries"][0]["values"] as $property => $value) {

                    if(!in_array($property, $ignoredCIUFields) && is_int($value) && $value != 0) {
                        $impactCounts[] = array("field" => $property, "counts" => $value);
                    }

                }

            }
            
            $returnData = array(
                "inc"           =>  $ticketNumber,
                "type"          =>  $ticketType,
                "sum"           =>  @$getINCData["entries"][0]["values"]["Description"],
                "sub"           =>  $ticketSubmitter,
                "next"          =>  getNextDate($ETR),
                "last"          =>  getLastUpdate($getWorkData["entries"]),
                "last_sub"      =>  $lastSubmitter,
                "due"           =>  getNextUpdateTime($ETR),
                "market"        =>  $market,
                "ciutype"       =>  $ticketCIUType,
                "last_up"       =>  $lastUpdate,
                "ce"            =>  $customerExperience,
                "impact"        =>  $impactCounts,
                "status"        =>  $ticketStatus,
                "reported"      =>  getNextDate(@$getINCData["entries"][0]["values"]["Submit Date"]),
                "dur"           =>  getDuration($start, $end),
                "res"           =>  @$getINCData["entries"][0]["values"]["Resolution"],
                "event_type"    =>  $eventType
            );

            echo json_encode($returnData);
            break;


        /**
         * Action: getTimeline
         * 
         * Gets data for the "TicketTimeline" form
         * 
         * @param ticket Ticket #
         * @return JSON Returns timeline data
         * 
         */
        case "getTimeline":
            $ticketNumber = $postData->ticket;
            $returnData = array();

            // Get work log entries for ticket
            $WorkParams = array("q" => "'Incident Number' = \"$ticketNumber\" AND ('View Access' = \"Public\" OR 'Work Log Type' = \"Resolution Communications\" OR 'Work Log Type' = \"Escalation\" OR 'Work Log Type' = \"Initial Dispatch Contact\")");
            $getWorkData = queryRemedy(
                "WorkInfo",
                $WorkParams,
                "Detailed Description,Work Log Submit Date,Work Log Type,View Access,Submitter"
            );

            // Get INC, CRQ, PBI, or PKE tickets related to this ticket
            $RelateParams = array("q" => "'Incident Number' = \"$ticketNumber\" AND ('Request Type01' = \"Incident\" OR 'Request Type01' = \"Infrastructure Change\" OR 'Request Type01' = \"Known Error\" OR 'Request Type01' = \"Problem Investigation\")");
            $getRelated = queryRemedy(
                "HPD:IncidentRelationshipInterface",
                $RelateParams,
                "Association Date__c,Request Type01,Submit Date,Request Description01,Request ID01"
            );

            // Loop through work log entries and populate returned data
            foreach($getWorkData["entries"] as $key => $val) {
                $sortDate = strtotime($val["values"]["Work Log Submit Date"]); // only used to sort $returnData before echo
                $submittedDate = date("l, F d Y g:iA", strtotime($val["values"]["Work Log Submit Date"]));
                $workNote = $val["values"]["Detailed Description"];
                $workType = $val["values"]["Work Log Type"];
                $isPublic = ($val["values"]["View Access"] == "Public") ? true:false;
                $thisTitle = "";
                $thisClass = "";
                $thisIcon = "";
                $thisUpdatedBy = $val["values"]["Submitter"];

                if($isPublic && $workType != "Resolution Communications") {
                    // Normal outage update
                    $thisTitle = "Update";
                    $thisClass = "primary";
                    $thisIcon = "fas fa-arrow-up";
                } else if($workType == "Resolution Communications") {
                    // Resolution of ticket
                    $thisTitle = "Resolution";
                    $thisClass = "success";
                    $thisIcon = "fas fa-check";
                } else if($workType == "Initial Dispatch Contact") {
                    // Initial dispatch
                    $thisTitle = "Initial Dispatch";
                    $thisClass = "info";
                    $thisIcon = "far fa-lightbulb";
                    $workNote = "Initial dispatch requested.";
                } else if($workType == "Escalation") {
                    // Ticket escalated
                    $thisTitle = "Escalation";
                    $thisClass = "danger";
                    $thisIcon = "fas fa-user-clock";
                    $workNote = "One or more escalations have been obtained.";
                }

                $returnData[] = array(
                    "sort"      =>  $sortDate,
                    "sub"       =>  $submittedDate,
                    "sub_by"    =>  $thisUpdatedBy,
                    "note"      =>  $workNote,
                    "title"     =>  $thisTitle,
                    "class"     =>  $thisClass,
                    "icon"      =>  $thisIcon
                );
            }

            // Loop through ticket relations and do the same
            if(count($getRelated["entries"]) > 0) {
                $previousTime = 50;

                foreach($getRelated["entries"] as $key => $val) {
                    $time = strtotime($val["values"]["Association Date__c"]);
                    $seconds = $time - $previousTime;
                    $relatedTime = date("l, F d Y g:iA", strtotime($val["values"]["Association Date__c"]));
                    $requestType = $val["values"]["Request Type01"];
                    $thisDescription = $val["values"]["Request Description01"];
                    $requestID = $val["values"]["Request ID01"];
                    $thisLink = "";

                    if($requestType == "Incident") {
                        $thisLink = "http://remedy.mediacomcorp.com/arsys/servlet/ViewFormServlet?form=HPD%3aHelp%20Desk&server=arprod&qual=%271000000161%27=%22$requestID%22";
                    } else if($requestType == "Infrastructure Change") {
                        $thisLink = "http://remedy.mediacomcorp.com/arsys/servlet/ViewFormServlet?form=CHG%3aInfrastructure%20Change&server=arprod&qual=%271000000182%27=%22$requestID%22";
                    } else if($requestType == "Problem Investigation") {
                        $thisLink = "http://remedy.mediacomcorp.com/arsys/servlet/ViewFormServlet?form=PBM%3aProblem%20Investigation&server=arprod&qual=%271000000232%27=%22$requestID%22";
                    } else {
                        $thisLink = "http://remedy.mediacomcorp.com/arsys/servlet/ViewFormServlet?form=PBM%3aKnown%20Error&server=arprod&qual=%271000000979%27=%22$requestID%22";
                    }

                    // $seconds avoids mass-related tickets being dumped into the timeline
                    if($seconds > 10) {
                        $returnData[] = array(
                            "sort"      =>  $time,
                            "sub"       =>  $relatedTime,
                            "note"      =>  $thisDescription,
                            "title"     =>  "Ticket Relation",
                            "class"     =>  "",
                            "icon"      =>  "fas fa-retweet",
                            "link"      =>  $thisLink,
                            "sub_by"    =>  "(none)"
                        );
                    }

                    $previousTime = $time;
                }
            }

            sortThis("sort", $returnData, "desc");
            echo json_encode($returnData);
            break;



        /**
         * Action: getHandoff
         * 
         * Gets data for the "GenerateHandoff" form
         * 
         * @param ticket Ticket #
         * @return JSON Returns handoff data
         * 
         */
        case "getHandoff":
            $returnData = array("active" => array(), "closed" => array());
            $dt = new DateTime();
            $minus9Hrs = $dt->modify("-9 hour");
            $minus9Hrs = $minus9Hrs->format(DateTime::ATOM);

            // Active alerts
            $activeParams = array(
                "q"     =>  "'Unavailability Class' = \"Incident\" AND 'Unavailability Status' = \"Current Unavailability\" AND 'Data Set Id' = \"BMC.ASSET\"", 
                "sort"  =>  "Actual Start Date.desc"
            );

            $getActives = queryRemedy(
                "CIU",
                $activeParams,
                "Description,Cross Reference ID,Actual Start Date"
            );

            // Closed alerts
            $closedParams = array(
                "q"     =>  "'Unavailability Class' = \"Incident\" AND 'Unavailability Status' = \"Restored\" AND 'Data Set Id' = \"BMC.ASSET\" AND 'Actual End Date' >= \"$minus9Hrs\"", 
                "sort"  =>  "Actual End Date.desc"
            );

            $getClosed = queryRemedy(
                "CIU",
                $closedParams,
                "Description,Cross Reference ID,Actual Start Date,Actual End Date"
            );

            // Process active alerts
            foreach($getActives["entries"] as $key => $val) {
                $INC = $val["values"]["Cross Reference ID"];

                // Get most recent work log entry (if any)
                $workParams = array(
                    "q"     =>  "'Incident Number' = \"$INC\" AND 'View Access' = \"Public\"", 
                    "limit" =>  1
                );

                $getWork = queryRemedy(
                    "WorkInfo",
                    $workParams,
                    "Submit Date"
                );

                @$lastModified = date("n/j/y g:i A", strtotime($getWork["entries"][0]["values"]["Submit Date"]));
                $startTime = date("n/j/y g:i A", strtotime($val["values"]["Actual Start Date"]));

                if($lastModified == "12/31/69 6:00 PM") {
                    $lastModified = "None";
                }

                $returnData["active"][] = array(
                    "INC"   =>  $INC,
                    "start" =>  $startTime,
                    "lmod"  =>  $lastModified,
                    "sum"   =>  $val["values"]["Description"]
                );
            }

            // Process closed alerts
            if(@count($getClosed["entries"]) > 0) {

                foreach($getClosed["entries"] as $key => $val) {
                    $returnData["closed"][] = array(
                        "INC"   =>  $val["values"]["Cross Reference ID"],
                        "start" =>  date("n/j/y g:i A", strtotime($val["values"]["Actual Start Date"])),
                        "end"   =>  date("n/j/y g:i A", strtotime($val["values"]["Actual End Date"])),
                        "sum"   =>  $val["values"]["Description"]
                    );
                }

            }

            echo json_encode($returnData);
            break;


        
        /**
         * Action: generateHandoff
         * 
         * Generates a shift handoff and returns data
         * 
         * @param tickets Selected tickets to run against the handoff
         * @return JSON Returns handoff data
         * 
         */
        case "generateHandoff":
            $tickets = $postData->tickets;
            $hour = date("G");
            $now = date("n/j/y");
            $active = array();
            $closed = array();
            $returnData = array(
                "subject"   =>  "",
                "body"      =>  ""
            );

            // Create subject line
            if($hour > 8 && $hour <= 16) {
				$returnData["subject"] = "1st Shift Handoff - ".$now;
			} else if($hour > 16) {
				$returnData["subject"] = "2nd Shift Handoff - ".$now;
			} else if($hour > 0 && $hour <= 8) {
				$returnData["subject"] = "3rd Shift Handoff - ".$now;
			}

            // Get ticket data
            foreach($tickets as $ticket) {
				$q = array("q" => "'Cross Reference ID' = \"$ticket\" AND 'Data Set Id' = \"BMC.ASSET\"");
				$fetchCIU = queryRemedy("CIU", $q, "Description,Unavailability Status,Actual Start Date,Actual End Date,Actual Duration Seconds");

				$q1 = array("q" => "'Incident Number' = \"$ticket\"");
				$fetchInfo = queryRemedy("HPD:Help%20Desk", $q1, "Latest Public Log");

				$start = date("n/j/y g:i A", strtotime($fetchCIU["entries"][0]["values"]["Actual Start Date"]));
				$end = date("n/j/y g:i A", strtotime($fetchCIU["entries"][0]["values"]["Actual End Date"]));

				$status = preg_replace( "/\r|\n/", "", $fetchInfo["entries"][0]["values"]["Latest Public Log"]);

				if($fetchCIU["entries"][0]["values"]["Unavailability Status"] != "Restored") {
					$duration = intval((strtotime($now) - strtotime($fetchCIU["entries"][0]["values"]["Actual Start Date"]))/60);
					$duration_hours = intval($duration / 60)."H";
					$duration_minutes = intval($duration % 60). "M";
					$active[] = $ticket." - ".$fetchCIU["entries"][0]["values"]["Description"]."\n".trim($status)."\nStart: ".$start."     Duration: ".$duration_hours." ".$duration_minutes."\n\n";
				} else {
					$duration = intval((strtotime($end) - strtotime($fetchCIU["entries"][0]["values"]["Actual Start Date"]))/60);
					$duration_hours = intval($duration / 60)."H";
					$duration_minutes = intval($duration % 60). "M";
					$closed[] = $ticket." - ".$fetchCIU["entries"][0]["values"]["Description"]."\n".trim($status)."\nStart: ".$start."     End: ".$end."     Duration: ".$duration_hours." ".$duration_minutes."\n\n";
				}
			}

            // Build body text
            $returnData["body"] = "--- Active (".count($active).") ---\n\n";
		
			foreach($active as $val) {
				$returnData["body"] .= $val;
			}

			if(count($active) == 0) {
				$returnData["body"] .= "None\n\n";
			}

			$returnData["body"] .= "--- FYI ---\n\n\n";

			$returnData["body"] .= "--- Closed (".count($closed).") ---\n\n";

			foreach($closed as $val) {
				$returnData["body"] .= $val;
			}

			if(count($closed) == 0) {
				$returnData["body"] .= "None\n\n";
			}

            echo json_encode($returnData);
            break;
    }
}