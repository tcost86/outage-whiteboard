/**
 *  Sends and returns data for POST requests
 *  Expects data to be JSON for sending and receiving
 * 
 *  Usage:
 *  let data = {
 *      action: 'action here in config.php',
 *      <data object 1>: <data value 1>,
 *      <data object 2>: <data value 2>,
 *      etc.
 *  }
 * 
 *  postData(data)
 *      .then(data => { do things with returned data here })
 *
 *  @param {object} data Data to be sent.
 *  @return {object} response JSON encoded response.
 */
async function postData(data = {}) {

    const response = await fetch('./php/config.php', {
        method: 'POST',
        mode: 'same-origin',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })

    return response.json()
}


/**
 *  Sets cookies 
 * 
 *
 *  @param cname Cookie name
 *  @param cvalue Cookie value
 *  @param cdays Expiration # days
 *  @return null
 */
function setCookie(cname, cvalue, cdays) {
    let d = new Date()
    d.setTime(d.getTime() + (cdays * 24 * 60 * 60 * 1000))
    let expires = 'expires=' + d.toUTCString()
    document.cookie = cname + '=' + cvalue + '; ' + expires
}


/**
 *  Retrieves cookies
 * 
 *
 *  @param cname Cookie name
 *  @return Returns cookie value
 */
function getCookie(cname) {
    let name = cname + "="
    let ca = document.cookie.split(';')
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i]
        while (c.charAt(0) == ' ') c = c.substring(1)
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length)
    }
    return ''
}