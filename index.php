<?php require "./php/session-check.php"; ?>
<!DOCTYPE html>
<html lang="en" class="has-navbar-fixed-top" style="overflow: hidden;">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/common/images/noc.ico" type="image/x-icon">
    <link rel="stylesheet" href="/common/css/bulma.min.css">
    <link rel="stylesheet" href="/common/css/fontawesome.css">
    <link rel="stylesheet" href="/common/css/protip.css">
    <link rel="stylesheet" href="/common/css/modal-fx.min.css">
    <link rel="stylesheet"  href="./css/default.css">
    <link rel="stylesheet"  href="./css/timeline.css">
    <title>NOC Outage Whiteboard</title>
</head>

<body>

<div id="app">
    <nav class="navbar is-blue has-text-white is-fixed-top" role="navigation" aria-label="main navigation">
        <a class="navbar-item" href="index.php">NOC Outage Whiteboard</a>
        <div class="divider">|</div>
        <div class="navbar-start">
            <router-link :to="{name: 'ActiveOutages'}" class="navbar-item"><span class="icon"><i class="fas fa-bell"></i></span><span>Active Alerts</span></router-link>
            <router-link :to="{name: 'ClosedOutages'}" class="navbar-item"><span class="icon"><i class="fas fa-bell-slash"></i></span><span>Closed Alerts</span></router-link>
            <router-link :to="{name: 'ProblemList'}" class="navbar-item"><span class="icon"><i class="fas fa-info-circle"></i></span><span>Open Problems</span></router-link>
            <router-link v-if="perm > 0" :to="{name: 'GenerateHandoff'}" class="navbar-item"><span class="icon"><i class="fas fa-flag-checkered"></i></span><span>Generate Shift Handoff</span></router-link>
        </div>

        <div class="navbar-end">
            <div class="navbar-item">
                <p class="control has-icons-left" >
                    <input v-on:keyup.enter="doSearch" v-model="dbSearchText" style="width: 20em;" class="input is-small" type="text" placeholder="Search the NOC DB" autocomplete="off">
                    <span class="icon is-small is-left"><i class="fas fa-search"></i>
                </p>
            </div>

            <div v-if="user != ''" class="navbar-item">
                <div class="control">
                    <a class="button is-small" style="color: black !important" href="/login/logout.php?r=/suite/owb_test/">
                        <span>Logout</span>
                        <span class="icon"><i class="fas fa-sign-out-alt"></i></span>
                    </a>
                </div>
            </div>

            <div v-else class="navbar-item">
                <div class="control">
                    <a class="button is-small" style="color: black !important" href="/login/index.php?r=/suite/owb_test/">
                        <span>Login</span>
                        <span class="icon"><i class="fas fa-sign-in-alt"></i></span>
                    </a>
                </div>
            </div>
        </div>
    </nav>

    <div>
        <keep-alive>
            <router-view :user="user" :perm="perm"></router-view>
        </keep-alive>
    </div>
</div>

<?php if($_SERVER["HTTP_HOST"] == "nocportal-t" || $_SERVER["HTTP_HOST"] == "nocportal-t.mediacomcorp.com") { ?>
    <script src="/common/js/vue.dev.js"></script>
<?php } else { ?>
    <script src="/common/js/vue.prod.js"></script>
<?php } ?>
<script src="/common/js/jquery.js"></script>
<script src="/common/js/vue.loader.js"></script>
<script src="/common/js/vue.router.js"></script>
<script src="/common/js/protip.js"></script>
<script src="./js/date.js"></script>
<script src="./js/utils.js"></script>

<script>
$(document).ready(function () {
    $.protip()
})

// Router and App setup
let routes = [{
        path: '/',
        redirect: '/active'
    },
    {
        path: '/active',
        name: 'ActiveOutages',
        component: httpVueLoader('./components/ActiveOutages.vue')
    },
    {
        path: '/closed',
        name: 'ClosedOutages',
        component: httpVueLoader('./components/ClosedOutages.vue')
    },
    {
        path: '/pbi',
        name: 'ProblemList',
        component: httpVueLoader('./components/ProblemList.vue')
    },
    {
        path: '/handoff',
        name: 'GenerateHandoff',
        component: httpVueLoader('./components/GenerateHandoff.vue')
    }
]

const router = new VueRouter({
    routes: routes
})

const app = new Vue({
    router: router,
    data: function() {
        return {
            user: '<?php echo $user; ?>',
            perm: <?php echo $perm; ?>,
            dbSearchText: ''
        }
	},
    methods: {
        doSearch() {
            let term = this.dbSearchText

            if(term != '') {
                let a = document.createElement('a')
                a.target = '_blank'
                a.href = '/database/search.php?q='+term
                a.click()
                a.remove()
                this.dbSearchText = ''
            }
        }
    }
}).$mount('#app')
</script>

</body>
</html>